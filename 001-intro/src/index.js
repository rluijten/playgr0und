import { TimelineLite, Power1 } from 'gsap';

function init() {
  const slides = document.querySelectorAll('.slider__slide');
  const slide1 = document.querySelector('.slider__slide--1');
  const slide2 = document.querySelector('.slider__slide--2');
  const slide3 = document.querySelector('.slider__slide--3');
  const slide4 = document.querySelector('.slider__slide--4');
  const slide5 = document.querySelector('.slider__slide--5');

  if (!slides.length) return;

  const tl = new TimelineLite();

  setTimeout(() => {
    tl.to(slide1, 0.7, { height: '100%', top: 0, bottom: 'auto', ease: Power1.easeInOut });
    tl.to(slide1, 0.7, { height: 0, ease: Power1.easeInOut, delay: 0.75 }, '-=0.4');

    tl.to(slide2, 0.7, { height: '100%', top: 0, bottom: 'auto', ease: Power1.easeInOut }, '-=1');
    tl.to(slide2, 0.7, { height: 0, ease: Power1.easeInOut }, '-=0.4');

    tl.to(slide3, 0.7, { height: '100%', top: 0, bottom: 'auto', ease: Power1.easeInOut }, '-=1');
    tl.to(slide3, 0.7, { height: 0, ease: Power1.easeInOut }, '-=0.4');

    tl.to(slide4, 0.7, { height: '100%', top: 0, bottom: 'auto', ease: Power1.easeInOut }, '-=1');
    tl.to(slide4, 0.7, { height: 0, ease: Power1.easeInOut }, '-=0.4');

    tl.to(slide5, 0.7, { height: '100%', top: 0, bottom: 'auto', ease: Power1.easeInOut }, '-=1 ');
    tl.to(slide5, 0.7, { height: 0, ease: Power1.easeInOut }, '-=0.4');
  }, 2000);
}

init();
