import React from 'react';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';

// typically you want to use `next/link` for this usecase
// but this example shows how you can also access the router
// using the withRouter utility.

const ActiveLink = ({ children, router, href }) => {
  const style = {
    marginRight: 10,
    color: router.pathname === href ? 'red' : 'black'
  };

  const handleClick = (e) => {
    e.preventDefault();
    router.push(href);
  };

  return (
    <a href={href} onClick={handleClick} style={style}>
      {children}
    </a>
  );
};

ActiveLink.propTypes = {
  children: PropTypes.node.isRequired,
  router: PropTypes.object.isRequired,
  href: PropTypes.string.isRequired
};

export default withRouter(ActiveLink);
