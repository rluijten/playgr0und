import React from 'react';
// import Link from 'next/link';

import ActiveLink from './ActiveLink';

// example header component
export default () => (
  <nav>
    <ActiveLink href="/">Home</ActiveLink>
    <ActiveLink href="/about">About</ActiveLink>
    <ActiveLink href="/contact">Contact</ActiveLink>
  </nav>
);
