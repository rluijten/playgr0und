import { h, app } from 'hyperapp';
// @jsx h

const state = {
  questions: [
    { question: "What is 2 + 2?", answer: 4, answerIsOpen: false },
    { question: "What is 2 - 2?", answer: 0, answerIsOpen: false }
  ]
}

const actions = {
  toggleAnswer: ({ question, answerIsOpen }) => state => ({
    questions: state.questions.map(
      q => (q.question === question ? { ...q, answerIsOpen: !answerIsOpen } : q)
    )
  })
}

const Question = ({ question, answer, answerIsOpen }) => (state, actions) => (
  <li>
    <h2>{question}</h2>
    <button onclick={() => actions.toggleAnswer({ question, answerIsOpen })}>
      {answerIsOpen ? "Hide" : "Show"} answer
    </button>
    {answerIsOpen && <p>{answer}</p>}
  </li>
)

const view = (state, actions) => (
  <ul>{state.questions.map(q => <Question {...q} />)}</ul>
)

app(state, actions, view, document.body)
