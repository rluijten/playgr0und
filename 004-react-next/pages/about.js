import React from 'react';
import Link from 'next/link';

import Nav from '../components/Nav';

export default () => (
  <div>
    <Nav />

    <h1>About page.</h1>

    Click
    {' '}
    <Link href="/"><a>here</a></Link>
    {' '}
    to go to home
  </div>
);
