import React from 'react';

import Nav from '../components/Nav';

function formatName(user) {
  return `${user.firstName} ${user.lastName}`;
}

const user = {
  firstName: 'Helmut',
  lastName: 'Fritz'
};

export default () => (
  <div>
    <Nav />

    <h1>
      Home page.
    </h1>

    <p>
      Hello.
      {' '}
      {formatName(user)}
      !
    </p>
  </div>
);
