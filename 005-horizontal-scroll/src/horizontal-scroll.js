import Smooth from 'smooth-scrolling';

// this.vars.width = window.innerWidth;
// this.dom.els = document.querySelectorAll('.element');
// this.dom.scroll = scroll container;

class HorizontalScroll extends Smooth {
  constructor(opt) {
    super(opt);

    this.createExtraBound();

    this.resizing = false;
    this.cache = null;
    this.dom.els = Array.prototype.slice.call(opt.els, 0);
  }

  createExtraBound() {
    ['getCache', 'inViewport'].forEach((fn) => this[fn] = this[fn].bind(this));
  }

  resize() {
    this.resizing = true;

    this.getCache();
    super.resize();

    console.log(this.dom.scroll, this.vars.bounding);

    this.dom.scroll.style.width = '';
    this.dom.scroll.style.height = `${this.vars.bounding}px`;
    this.resizing = false;
  }

  getCache() {
    this.cache = [];

    const unit = this.vars.width / 5;

    this.dom.els.forEach((el, index) => {
      el.style.display = 'inline-block';
      el.style.transform = 'none';
      el.style.width = `${unit}px`;

      const scrollX = this.vars.target;
      const bounding = el.getBoundingClientRect();
      const bounds = {
        el: el,
        state: true,
        left: bounding.left + scrollX,
        right: bounding.right + scrollX,
        center: unit / 2
      };

      this.cache.push(bounds);
    });

    this.dom.section.style.width = `${this.vars.width}px`;
    this.vars.bounding = (unit * this.dom.els.length - 200);
  }

  run() {
    this.dom.els.forEach(this.inViewport);
    this.dom.section.style[this.prefix] = `translate3d(${this.vars.current * -1}px, 0, 0)`;

    super.run();
  }

  inViewport(el, index) {
    if(!this.cache || this.resizing) return;

    const cache = this.cache[index];
    const current = this.vars.current;
    const left = Math.round(cache.left - current);
    const right = Math.round(cache.right - current);
    const inView = right > 0 && left < this.vars.width;

    if(inView) {
      if(!el.state) {
        el.state = true

        if (!el.classList.contains('in-view')) el.classList.add('in-view');
      }
    } else {
      el.state = false

      if (el.classList.contains('in-view')) el.classList.remove('in-view');
    }
  }
}

export default HorizontalScroll;
