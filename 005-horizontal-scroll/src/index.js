import { TweenMax } from 'gsap';

import HorizontalScroll from './horizontal-scroll';

function init() {
  const horizontalScroll = new HorizontalScroll({
    preload: false,
    native: true,
    direction: 'vertical',
    section: document.querySelector('.container'),
    els: document.querySelectorAll('.element'),
    ease: 0.1
  })

  horizontalScroll.init();
}

init();
