// var texture, material, plane;
//
// texture = THREE.ImageUtils.loadTexture( "../img/texture.jpg" );
//
// // assuming you want the texture to repeat in both directions:
// texture.wrapS = THREE.RepeatWrapping;
// texture.wrapT = THREE.RepeatWrapping;
//
// // how many times to repeat in each direction; the default is (1,1),
// //   which is probably why your example wasn't working
// texture.repeat.set( 4, 4 );
//
// material = new THREE.MeshLambertMaterial({ map : texture });
// plane = new THREE.Mesh(new THREE.PlaneGeometry(400, 3500), material);
// plane.material.side = THREE.DoubleSide;
// plane.position.x = 100;
//
// // rotation.z is rotation around the z-axis, measured in radians (rather than degrees)
// // Math.PI = 180 degrees, Math.PI / 2 = 90 degrees, etc.
// plane.rotation.z = Math.PI / 2;
//
// scene.add(plane);

import * as THREE from 'three';

import map from '../img/map.jpg';

const OrbitControls = require('three-orbit-controls')(THREE);

export default class Globe {

  constructor () {
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.init();
  }

  init () {
    // earth params
    this.radius = 0.5;
    this.segments = 50;
    this.rotation = 30;

    // scene
    this.scene = new THREE.Scene();

    // camera
    this.camera = new THREE.PerspectiveCamera(45, this.width / this.height, 0.01, 1000);
    this.camera.position.z = 1;

    // renderer
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(this.width, this.height);

    // light
    this.scene.add(new THREE.AmbientLight(0x333333));

    this.light = new THREE.DirectionalLight(0xffffff, 1);
    this.light.position.set(5, 3, 5);
    this.scene.add(this.light);

    // map texture
    this.texture = THREE.ImageUtils.loadTexture(map);
    this.texture.wrapS = THREE.RepeatWrapping;
    this.texture.wrapT = THREE.RepeatWrapping;
    // this.texture.repeat.set( 4, 4 );

    this.material = new THREE.MeshLambertMaterial({ map : this.texture });
    this.plane = new THREE.Mesh(new THREE.PlaneGeometry(400, 3500), this.material);
    this.plane.material.side = THREE.DoubleSide;
    this.plane.position.x = 100;

    // rotation.z is rotation around the z-axis, measured in radians (rather than degrees)
    // Math.PI = 180 degrees, Math.PI / 2 = 90 degrees, etc.
    // this.plane.rotation.z = Math.PI / 2;

    this.scene.add(this.plane);

    // controls
    this.controls = new OrbitControls(this.camera);

    document.body.appendChild(this.renderer.domElement);

    // render
    this.render();

    // resize
    window.addEventListener('resize', () => {
      this.onResize();
    }, false);
  }

  render () {
    this.controls.update();

    requestAnimationFrame(() => {
      this.render();
    });

    this.animate();

    this.renderer.render(this.scene, this.camera);
  }

  animate() {
    // this.sphere.rotation.y += 0.0005;
    // this.clouds.rotation.y += 0.0005;
  }

  createGlobe (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius, segments, segments),
      new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture(earthNoClouds),
        bumpMap: THREE.ImageUtils.loadTexture(earthBump),
        bumpScale: 0.005,
        specularMap: THREE.ImageUtils.loadTexture(earthWater)
      })
    );
  }

  createClouds (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius + 0.003, segments, segments),
      new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture(earthClouds),
        opacity: 0.5,
        transparent: true
      })
    );
  }

  createStars (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius, segments, segments),
      new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture(stars),
        side: THREE.BackSide
      })
    );
  }

  onResize () {
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width, this.height);
  }
}
