import { TweenMax, TimelineMax, Expo } from 'gsap';

class Slider {
  constructor() {
    this.dom = {
      slider: document.querySelector('.js-slider'),
      slides: document.querySelectorAll('.js-slide'),
      heading: document.querySelector('.js-heading')
    };

    this.data = {
      current: 0,
      next: 1,
      total: this.dom.slides.length - 1
    };

    this.state = {
      animating: false
    };
  }

  setStyles() {
    [...this.dom.slides].forEach((slide, index) => {
      if (index === 0) {
        TweenMax.set(slide, { zIndex: index });

        this.dom.heading.innerHTML = slide.getAttribute('data-heading');

        return;
      }

      const masks = slide.querySelectorAll('.js-mask');

      TweenMax.set(slide, { zIndex: index });
      TweenMax.set(masks, { xPercent: 100 });
    });
  }

  transitionNext() {
    const current = this.dom.slides[this.data.current];
    const next = this.dom.slides[this.data.next];
    const currentMasks = current.querySelectorAll('.js-mask');
    const nextMasks = next.querySelectorAll('.js-mask');
    const nextHeading = next.getAttribute('data-heading');

    console.log(nextHeading);

    const tl = new TimelineMax({ paused: true });

    tl
      .add('start')
      // animate out masks of current slide
      .fromTo(
        currentMasks[0],
        1.8,
        { xPercent: 0 },
        { xPercent: -100, ease: Expo.easeOut },
        'start+=0.3'
      )
      .fromTo(
        currentMasks[1],
        1.8,
        { xPercent: 0 },
        { xPercent: -100, ease: Expo.easeOut },
        'start'
      )
      .fromTo(
        currentMasks[2],
        1.8,
        { xPercent: 0 },
        { xPercent: -100, ease: Expo.easeOut },
        'start+=0.15'
      )
      .fromTo(
        currentMasks[3],
        1.8,
        { xPercent: 0 },
        { xPercent: -100, ease: Expo.easeOut },
        'start+=0.05'
      )
      .fromTo(
        currentMasks[4],
        1.8,
        { xPercent: 0 },
        { xPercent: -100, ease: Expo.easeOut },
        'start+=0.2'
      )

      // animate out current heading
      .fromTo(
        this.dom.heading,
        0.4,
        { autoAlpha: 1 },
        { autoAlpha: 0 },
        'start'
      )
      .set(
        this.dom.heading,
        {
          y: 50,
          autoAlpha: 1,
          onComplete: () => {
            this.dom.heading.innerHTML = nextHeading;
          }
        },
        'start+=0.4'
      )

      // animate in next heading
      .fromTo(
        this.dom.heading,
        0.7,
        { y: 50 },
        { y: 0, autoAlpha: 1, ease: Expo.easeInOut },
        'start+=0.4'
      )

      // animate in masks of next slide
      .fromTo(
        nextMasks[0],
        1.5,
        { xPercent: 100 },
        { xPercent: 0, ease: Expo.easeOut },
        'start+=0.15'
      )
      .fromTo(
        nextMasks[1],
        1.5,
        { xPercent: 100 },
        { xPercent: 0, ease: Expo.easeOut },
        'start'
      )
      .fromTo(
        nextMasks[2],
        1.5,
        { xPercent: 100 },
        { xPercent: 0, ease: Expo.easeOut },
        'start+=0.08'
      )
      .fromTo(
        nextMasks[3],
        1.5,
        { xPercent: 100 },
        { xPercent: 0, ease: Expo.easeOut },
        'start+=0.03'
      )
      .fromTo(
        nextMasks[4],
        1.5,
        { xPercent: 100 },
        {
          xPercent: 0,
          ease: Expo.easeOut,
          onComplete: () => {
            this.state.animating = false;
          }
        },
        'start+=0.1'
      );

    tl.play();
  }

  nextSlide = () => {
    console.log(this.state.animating)

    if (this.state.animating) return;

    this.state.animating = true;

    this.transitionNext();

    this.data.current = this.data.current === this.data.total ? 0 : this.data.current + 1;
    this.data.next = this.data.current === this.data.total ? 0 : this.data.current + 1;

    console.log(this.data.current, this.data.next);
  }

  listeners() {
    document.body.addEventListener('click', this.nextSlide, { passive: true });
  }

  init() {
    this.setStyles();
    this.listeners();
  }
}

const slider = new Slider();
slider.init();
