import { TweenMax, TimelineMax, Expo } from 'gsap';

class Header {
  constructor() {
    this.dom = {
      header: document.querySelector('.js-header'),
      layers: document.querySelectorAll('.js-layer')
    };

    this.data = {
      scrollEase: 0.2,
      scrollEaseLimit: 0.2
    }

    this.state = {
      scrollTarget: 0,
      scrollPos: 0
    }
  }

  scrollHandler = () => {
    this.state.scrollTarget = window.scrollY;
  }

  animate = () => {
    if (this.state.scrollTarget !== this.state.scrollPos) {
      if (Math.abs(this.state.scrollPos - this.state.scrollTarget) < this.data.scrollEaseLimit) {
        this.state.scrollPos = this.state.scrollTarget;
      }

      this.state.scrollPos += (this.state.scrollTarget - this.state.scrollPos) * this.data.scrollEase;

      let x;
      let y;
      let transform;
      [...this.dom.layers].forEach((layer) => {
        x = layer.classList.contains('header__title') ? '-50%' : 0;
        y = parseFloat(layer.getAttribute('data-y'), 10);
        transform = `translate3d(${x}, ${-(this.state.scrollPos * y)}px, 0)`;

        layer.style.transform = transform;
      });
    }
  }

  listeners() {
    window.addEventListener('scroll', this.scrollHandler, { passive: true });
  }

  update = () => {
    this.animate();
    window.requestAnimationFrame(this.update);
  }

  init() {
    this.listeners();
    this.update();
  }
}

class Article {
  constructor() {
    this.dom = {
      articles: document.querySelectorAll('.js-article')
    }
  }

  expand() {

  }

  listeners() {
    [...this.dom.articles].forEach((article) => {
      article.addEventListener('click', this.expand, { passive: true });
    });
  }

  init() {
    console.log('aRTICLE.');

    this.listeners();
  }
}

const header = new Header();
header.init();

const article = new Article();
article.init();
