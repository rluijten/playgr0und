import * as THREE from 'three';
import { TweenMax } from 'gsap';

import { FRAGMENT_SHADER, VERTEX_SHADER } from './shaders';

// styles
import './index.scss';

class App {
  constructor() {
    this.DOM = {
      container: document.getElementById('app'),
      els: document.querySelectorAll('.js-el')
    };
    
    this.state = {
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      // mouseX: 0,
      // mouseY: 0,
      display: 'grid',
      isAnimating: false,
      elIndex: -1
    }
    
    this.renderer = new THREE.WebGLRenderer({
      alpha: true,
      antialias: true
    });
    
    this.camera = new THREE.PerspectiveCamera(
      75,
      this.state.windowWidth / this.state.windowHeight,
      0.1,
      1000
    );

    this.scene = new THREE.Scene();
    this.loader = new THREE.TextureLoader();
    this.clock = new THREE.Clock();

    this.uniforms = {
      u_time: { type: "f", value: 0 },
      u_resolution: { type: "v2", value: new THREE.Vector2(this.width, this.height) },
      u_mouse: { type: "v2", value: new THREE.Vector2(0, 0) },
      u_text0: { type: "t", value: null },
      u_progress: { type: "f", value: 0 },
      u_meshScale: { type: "v2", value: new THREE.Vector2(0, 0) },
      u_meshPosition: { type: "v2", value: new THREE.Vector2(0, 0) },
      u_viewSize: { type: "v2", value: new THREE.Vector2(0, 0) },
      u_ratio: { type: "v2", value: new THREE.Vector2(0, 0) }
    };

    this.toGrid = this.toGrid.bind(this);
    this.resizeHandler = this.resizeHandler.bind(this);
    this.update = this.update.bind(this);
  }
  
  getViewSize() {
    const fovInRadians = this.camera.fov * Math.PI / 180;
    const height = Math.abs(
      this.camera.position.z * Math.tan(fovInRadians / 2) * 2
    );

    return { width: height * this.camera.aspect, height };
  }
  
  getPosition(ev){    
    this.uniforms.u_mouse.value.x = ev.clientX;
    this.uniforms.u_mouse.value.y = ev.clientY;
  }
  
  getAspectRatio(texture, viewSize, rect) {
    const textSize = {
      w: texture.image.width,
      h: texture.image.height
    };
    const canvasSize = {
      w: rect.width,
      h: rect.height
    };

    const aspectText = textSize.w / textSize.h;
    const aspCanvas = canvasSize.w / canvasSize.h;

    const ratio =
      aspCanvas > aspectText
        ? { x: 1, y: 1 / aspCanvas * aspectText }
        : { x: 1 * aspCanvas / aspectText, y: 1 };

    this.uniforms.u_ratio.value = ratio;
  }
  
  addMesh() {
    const geometry = new THREE.PlaneBufferGeometry(1, 1, 120, 120);
    const material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: VERTEX_SHADER,
      fragmentShader: FRAGMENT_SHADER,
      side: THREE.DoubleSide
    });
    
    this.mesh = new THREE.Mesh(geometry, material);

    this.scene.add(this.mesh);
  }

  updateMesh() {
    if (this.state.elIndex === -1) return;

    const item = this.DOM.els[this.state.elIndex];
    const rect = item.getBoundingClientRect();
    const viewSize = this.getViewSize();
    
    // 1. Transform pixel units to camera's view units
    const widthViewUnit = rect.width * viewSize.width / window.innerWidth;
    const heightViewUnit = rect.height * viewSize.height / window.innerHeight;
    let xViewUnit = rect.left * viewSize.width / window.innerWidth;
    let yViewUnit = rect.top * viewSize.height / window.innerHeight;

    // 2. Make units relative to center instead of the top left.
    xViewUnit = xViewUnit - viewSize.width / 2;
    yViewUnit = yViewUnit - viewSize.height / 2;

    // 3. Make the origin of the plane's position to be the center instead of top Left.
    let x = xViewUnit + widthViewUnit / 2;
    let y = -yViewUnit - heightViewUnit / 2;

    // 4. Scale and position mesh
    const mesh = this.mesh;
    // Since the geometry's size is 1. The scale is equivalent to the size.
    mesh.scale.x = widthViewUnit;
    mesh.scale.y = heightViewUnit;
    mesh.position.x = x;
    mesh.position.y = y;

    this.uniforms.u_meshPosition.value.x = x / widthViewUnit;
    this.uniforms.u_meshPosition.value.y = y / heightViewUnit;
    this.uniforms.u_meshScale.value.x = widthViewUnit;
    this.uniforms.u_meshScale.value.y = heightViewUnit;

    const img = item.src;
    
    this.loader.load(img, texture => {
      this.getAspectRatio(texture, viewSize, rect);
      this.uniforms.u_text0.value = texture;

      if (this.isResize) return;
      this.toFullScreen();
    });
  }
  
  toGrid() {
    if (this.state.display === 'grid' || this.state.isAnimating) return;
    
    this.state.isAnimating = true;
    
    this.tween = TweenMax.to(this.uniforms.u_progress, 1, {
      value: 0,
      onUpdate: this.render.bind(this),
      onComplete: () => {
        this.state.isAnimating = false;
        this.state.display = 'grid';
        this.DOM.container.style.zIndex = -1;
      }
    });
  }

  toFullScreen() {
    if (this.state.display === 'fullscreen' || this.state.isAnimating) return;
    
    this.state.isAnimating = true;
    this.DOM.container.style.zIndex = 2;
    this.tween = TweenMax.to(this.uniforms.u_progress, 1, {
      value: 1,
      onUpdate: this.render.bind(this),
      onComplete: () => {
        this.state.isAnimating = false;
        this.state.display = 'fullscreen';
        this.DOM.container.addEventListener('click', this.toGrid)
      }
    });
  }
  
  update() {
    this.uniforms.u_time.value = this.clock.getElapsedTime();

    this.render();
    requestAnimationFrame(this.update);
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }
  
  clickHandler(ev, elIndex) {
    this.state.elIndex = elIndex;

    this.isResize = false;
    this.getPosition(ev);
    this.updateMesh(ev, elIndex);
  }
  
  resizeHandler() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    
    this.uniforms.u_resolution.value.x = innerWidth
    this.uniforms.u_resolution.value.y = innerHeight

    this.updateMesh();
    this.render();

    this.isResize = true;
  }
  
  addListeners() {
    for (let [i, value] of this.DOM.els.entries()) {
      value.addEventListener('click', ev => this.clickHandler(ev, i));
    }

    window.addEventListener('scroll', this.resizeHandler);
    window.addEventListener('resize', this.resizeHandler);
  }

  init() {
    this.renderer.setSize(this.width, this.height);
    this.camera.position.set(0, 0, 1);
    this.scene.add(this.camera);

    this.DOM.container.appendChild(this.renderer.domElement);

    this.camera.position.z = 50;
    this.camera.lookAt = this.scene.position;

    const viewSize = this.getViewSize();
    this.uniforms.u_viewSize.value.x = viewSize.width;
    this.uniforms.u_viewSize.value.y = viewSize.height;

    this.addListeners();
    this.addMesh();
    this.resizeHandler();
    this.update();
  }
}

const app = new App();
app.init();
