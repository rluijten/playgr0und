import * as THREE from 'three'
import { TweenMax as TM } from 'gsap'

import vertexShader from '../shaders/vertex.glsl'
import fragmentShader from '../shaders/fragment.glsl'

export default class Figure {
  constructor(image, video, scene, raycaster, mouse) {
    this.DOM = {
      image,
      video
    };

    console.log(this.DOM.video);

    this.scene = scene

    this.loader = new THREE.TextureLoader()

    this.image = this.loader.load(this.DOM.image.src, this.start)
    
    // this.hover = this.loader.load(this.DOM.image.dataset.hover)

    this.video = new THREE.VideoTexture(video);
    this.video.minFilter = THREE.LinearFilter;
    this.video.magFilter = THREE.LinearFilter;
    this.video.format = THREE.RGBFormat;

    this.DOM.image.style.opacity = 0
    this.DOM.video.style.opacity = 0
    
    this.sizes = new THREE.Vector2(0, 0)
    this.offset = new THREE.Vector2(0, 0)

    this.raycaster = raycaster
    this.mouse = mouse

    window.addEventListener('mousemove', this.onMouseMove)
    window.addEventListener('scroll', this.onScroll)
  }

  start = () => {
    this.getSizes()
    this.createMesh()
    this.update()
  }

  getSizes() {
    const { width, height, top, left } = this.DOM.image.getBoundingClientRect()

    this.sizes.set(width, height)

    this.offset.set(
      left - window.innerWidth / 2 + width / 2,
      -top + window.innerHeight / 2 - height / 2
    )
  }

  createMesh() {
    this.uniforms = {
      u_image: { type: 't', value: this.image },
      u_imagehover: { type: 't', value: this.hover },
      u_video: { type: 't', value: this.video },
      u_mouse: { value: this.mouse },
      u_time: { value: 0 },
      u_res: {
        value: new THREE.Vector2(window.innerWidth, window.innerHeight)
      }
    }

    this.geometry = new THREE.PlaneBufferGeometry(1, 1, 1, 1)
    this.material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
      defines: {
        PR: window.devicePixelRatio.toFixed(1)
      },
      transparent: true,
      depthTest: false
    })

    this.mesh = new THREE.Mesh(this.geometry, this.material)

    this.mesh.position.set(this.offset.x, this.offset.y, 0)
    this.mesh.scale.set(this.sizes.x, this.sizes.y, 1)

    this.scene.add(this.mesh)
  }

  onMouseMove = (e) => {
    const { top, right, bottom, left } = this.DOM.image.getBoundingClientRect()

    // calculate objects intersecting the picking ray
    const intersects = this.raycaster.intersectObjects(this.scene.children)

    for (let i = 0; i < intersects.length; i += 1) {
      if (intersects[i].object === this.mesh) {
        // ((input - min) * 100) / (max - min)
        // const percentageX = ((e.clientX - left) * 100) / (right - left);
        // const percentageY = ((e.clientY - top) * 100) / (bottom - top);

        TM.to(this.mesh.rotation, 0.5, {
          x: -this.mouse.y * 0.2,
          y: this.mouse.x * 0.2
        })
      } else {
        TM.to(this.mesh.rotation, 0.5, {
          x: 0,
          y: 0
        })
      }
    }
  }

  onScroll = () => {
    this.getSizes()
    this.mesh.position.set(this.offset.x, this.offset.y, 0)
  }

  update() {
    // this.video.needsUpdate = true;
    // this.uniforms.u_time.value += 0.01
  }
}
