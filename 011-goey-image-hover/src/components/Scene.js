import * as THREE from 'three';
import { TweenMax as TM } from 'gsap'

import Figure from "./Figure";

const perspective = 800;

export default class Scene {
  constructor() {
    this.DOM = {
      container: document.getElementById('stage'),
      // images: document.querySelectorAll('.tile__image'),
      // videos: document.querySelectorAll('.tile__video')
      image: document.querySelector('.js-image'),
      video: document.querySelector('.js-video')
    }

    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.DOM.container,
      alpha: true
    });

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.initLights();
    this.initCamera();

    this.raycaster = new THREE.Raycaster();
    this.intersectObject = null;
    this.mouse = new THREE.Vector2(0, 0);

    // [...this.DOM.images].forEach((image, index) => {
    //   this.figure = new Figure(image, this.scene, this.raycaster, this.mouse);
    // });

    this.figure = new Figure(this.DOM.image, this.DOM.video, this.scene, this.raycaster, this.mouse);

    this.update();

    window.addEventListener('mousemove', this.onMouseMove, false);
  }

  initLights() {
    const ambientlight = new THREE.AmbientLight(0xffffff, 2);
    this.scene.add(ambientlight);
  }

  initCamera() {
    const fov =
      (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI;

    this.camera = new THREE.PerspectiveCamera(
      fov,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    this.camera.position.set(0, 0, perspective);
  }

  update = () => {
    if (
      this.renderer === undefined ||
      this.scene === undefined ||
      this.camera === undefined
    )
      return;

    // update the picking ray with the camera and mouse position
    this.raycaster.setFromCamera(this.mouse, this.camera);

    requestAnimationFrame(this.update);

    // this.figure.update();

    this.renderer.render(this.scene, this.camera);
  }

  onMouseMove = (e) => {
    TM.to(this.mouse, 0.5, {
      x: (e.clientX / window.innerWidth) * 2 - 1,
      y: -(e.clientY / window.innerHeight) * 2 + 1
    })
  }
}
