export const VERTEX_SHADER = `
    #define S(a,b,n) smoothstep(a,b,n)
    
    varying vec2 vUv;

    uniform float u_progress;

    uniform vec2 u_viewSize;
    uniform vec2 u_meshScale;
    uniform vec2 u_meshPosition;
    uniform vec2 u_mouse;
    uniform vec2 u_resolution;

    void main(){
      vUv = uv;
      vec3 pos = position;

      float activation = distance(uv, vec2(.5)) * 1.5;
      float lastStart = 0.5;
      float startAt = activation * lastStart;
      float vertexProgress =  S(startAt, 1., u_progress);

      vec2 scaleViewSize = u_viewSize / u_meshScale - 1.;
      vec2 scale = vec2(1. + scaleViewSize * vertexProgress);
      pos.xy *= scale;

      pos.x += -u_meshPosition.x * vertexProgress;
      pos.y += -u_meshPosition.y * vertexProgress;

      gl_Position = projectionMatrix * modelViewMatrix * vec4(pos, 1.);
    }
  `;

  export const FRAGMENT_SHADER = `
    #define PI 3.14159265359
    #define PI2 6.28318530718
    #define S(a,b,n) smoothstep(a,b,n)

    uniform float u_time;

    uniform vec2 u_screenSize;
    uniform vec2 u_imageSize;
    uniform vec2 u_ratio;

    uniform sampler2D u_text0;

    varying vec2 vUv;

    void main(){
      vec2 uv = vUv;

      vec4 img = texture2D(u_text0, (uv - .5) * u_ratio + .5);

      gl_FragColor = img;
    }
  `;

