import * as THREE from 'three';
// import { TweenMax as TM } from 'gsap';

import vertexShader from '../shaders/vertex.glsl';
import fragmentShader from '../shaders/fragment.glsl';

const perspective = 800;

export default class Scene {
  constructor() {
    this.DOM = {
      container: document.getElementById('stage')
    };

    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.DOM.container,
      alpha: true
    });

    this.uniforms = {
      u_time: { type: 'f', value: 0.2 },
      u_resolution: { type: 'v2', value: new THREE.Vector2() },
    };

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);
  }

  initCamera() {
    const fov =
      (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI;

    this.camera = new THREE.PerspectiveCamera(
      fov,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    this.camera.position.set(0, 0, perspective);
  }

  createCube() {
    const geometry = new THREE.BoxGeometry(50, 50, 50);
    const material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader
    });

    this.cube = new THREE.Mesh(geometry, material);
    this.cube.position.set(0, 0 , 0);

    this.scene.add(this.cube);
  }

  animateCube() {
    this.cube.rotation.x += 0.03;
    this.cube.rotation.y += 0.05;
    this.cube.rotation.z += 0.1;

    this.cube.position.y = Math.sin(this.uniforms.u_time.value) * 100;
    console.log(Math.sin(this.uniforms.u_time.value) * 100);
  }

  update = () => {
    if (
      this.renderer === undefined ||
      this.scene === undefined ||
      this.camera === undefined
    )
      return;

    this.uniforms.u_time.value += 0.01;

    this.animateCube();

    requestAnimationFrame(this.update);

    this.renderer.render(this.scene, this.camera);
  }

  init() {
    this.initCamera();
    this.createCube();
    this.update();
  }
}
