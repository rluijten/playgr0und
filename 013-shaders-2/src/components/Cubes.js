import * as THREE from 'three';
import * as BAS from 'three-bas';

console.log(BAS);

class Cubes {
  constructor(scene) {
    this.scene = scene;

    this.cubes = [];
    this.geometry;
    this.material;

    this.config = {
      // size of the grid in world units
      gridSize: 100,
      // length of each axis
      gridLength: 30,
      // translation delta on the x axis
      deltaX: 200,
      // animation duration for each mesh
      duration: 2.0,
      // startTime for each mesh will be based on the total delay
      totalDelay: 1.0
    };

    this.time = 0;
  }

  create() {
    // dispose previous cubes
    this.dispose();
    
    // determine size of each cube based size and length
    const cubeSize = this.config.gridSize / (this.config.gridLength * 1.5);
    const gridHalfSize = this.config.gridSize * 0.5;

    // total cubes based on grid length on each axis
    const cubeCount = this.config.gridLength * this.config.gridLength * this.config.gridLength;

    // create the geometry that will be repeated in the buffer geometry
    // I refer to this 'base' geometry as a prefab
    const prefab = new THREE.BoxGeometry(cubeSize, cubeSize, cubeSize);

    // create the buffer geometry where a set number of prefabs are repeated
    // PrefabBufferGeometry offers some utility methods for working with such geometries
    this.geometry = new BAS.PrefabBufferGeometry(prefab, cubeCount);

    // create a buffer for start positions per prefab, with an item size of 3 (x, y, z)
    const startPositionBuffer = this.geometry.createAttribute('startPosition', 3);
    // create a buffer for end positions per prefab, with an item size of 3 (x, y, z)
    const endPositionBuffer = this.geometry.createAttribute('endPosition', 3);
    // create a buffer for duration per prefab, with the item size of 1
    const durationBuffer = this.geometry.createAttribute('duration', 1);
    // create a buffer for start time per prefab, with the item size of 1
    const startTimeBuffer = this.geometry.createAttribute('startTime', 1);

    // populate the buffers

    let cubeIndex = 0;
    // reuse the same array each loop iteration
    const tmpa = [];

    for (let x = 0; x < this.config.gridLength; x++) {
      for (let y = 0; y < this.config.gridLength; y++) {
        for (let z = 0; z < this.config.gridLength; z++) {
  
          // calculate start position spread around the x, y, and z axes, offset by half delta on the x axis
          // the x, y and z values are stored in the temporary array
          tmpa[0] = THREE.Math.mapLinear(x, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize) - this.config.deltaX * 0.5;
          tmpa[1] = THREE.Math.mapLinear(y, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize);
          tmpa[2] = THREE.Math.mapLinear(z, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize);
          // geometry.setPrefabData will use the array to set the same values for each vertex in a prefab based on an index 
          this.geometry.setPrefabData(startPositionBuffer, cubeIndex, tmpa);
          
          // repeat the same steps for the end position
          tmpa[0] = THREE.Math.mapLinear(x, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize) + this.config.deltaX * 0.5;
          tmpa[1] = THREE.Math.mapLinear(y, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize);
          tmpa[2] = THREE.Math.mapLinear(z, 0, this.config.gridLength - 1, gridHalfSize, -gridHalfSize);
          
          this.geometry.setPrefabData(endPositionBuffer, cubeIndex, tmpa);
          
          // repeat the same steps for duration
          // in this example, the duration is the same for each prefab, so it could be set as a uniform
          tmpa[0] = this.config.duration;
      
          this.geometry.setPrefabData(durationBuffer, cubeIndex, tmpa);
          
          // repeat the sa5me steps for start time
          tmpa[0] = (this.config.totalDelay / cubeCount) * cubeIndex;
          
          this.geometry.setPrefabData(startTimeBuffer, cubeIndex, tmpa);
          
          // increment the cubeIndex for the next iteration
          cubeIndex++;
        }
      }
    }

    // create the animation material
    // it 'extends' THREE.MeshPhongMaterial by injecting arbitrary GLSL code at key places in the shader code
    this.material = new BAS.PhongAnimationMaterial({
      flatShading: THREE.FlatShading,
      // define a time uniform that will control the state of the animation
      // the uniform will be the same for each vertex
      uniforms: {
        time: {value: 0}
      },
      // add GLSL definitions for the uniform and the 4 attributes we defined on the geometry
      // the names and types must be the same as defined above
      // we use vec3 for attributes with an item size of 3
      // we use float for attributes with an item size of 1
      vertexParameters: [
        'uniform float time;',
        
        'attribute vec3 startPosition;',
        'attribute vec3 endPosition;',
        'attribute float startTime;',
        'attribute float duration;',
      ],
      // add the GLSL animation update logic
      vertexPosition: [
        // progress is calculated based on the time uniform, and the duration and startTime attributes
        'float progress = clamp(time - startTime, 0.0, duration) / duration;',
        // 'transformed' is a variable defined by THREE.js.
        // it is used throughout the vertex shader to transform the vertex position
        // 'mix' is a built-in GLSL method that performs linear interpolation
        'transformed += mix(startPosition, endPosition, progress);'
      ]
    });
    
    // once the geometry and metrials are defined we can use them to create one single mesh, and add it to the scene
    this.cubes = new THREE.Mesh(this.geometry, this.material);
    this.scene.add(this.cubes);
  }

  update() {
    // increment global time
    this.cubes.material.uniforms.time.value += 1/60;
    // reset time when it exceeds the total duration (plus a small delay)
    this.cubes.material.uniforms.time.value %= (this.config.duration + this.config.totalDelay + 1.0);
  }

  dispose() {
    this.cubes && this.scene.remove(this.cubes);
    this.geometry && this.geometry.dispose();
    this.material && this.material.dispose();
  }
}

export default Cubes;
