import * as THREE from 'three';
import Cubes from './Cubes';

export default class Scene {
  constructor() {
    this.DOM = {
      container: document.getElementById('stage')
    };

    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.DOM.container,
      alpha: true
    });

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.cubes = new Cubes(this.scene);

    this.raf = null;
  }

  initCamera() {
    const perspective = 800;
    const fov = (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI;

    this.camera = new THREE.PerspectiveCamera(
      fov,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );

    this.camera.position.set(0, 0, perspective);
  }

  initLights() {
    let light;

    light = new THREE.DirectionalLight(0xff0000, 1);
    light.position.set(-1, 1, 1);
    this.scene.add(light);

    light = new THREE.DirectionalLight(0x0000ff, 1);
    light.position.set(1, 1, 1);
    this.scene.add(light);

    light = new THREE.PointLight(0x00ff00, 1, 200);
    this.scene.add(light);
  }

  tick = () => {
    this.cubes.update();
    this.render();

    this.raf = window.requestAnimationFrame(this.tick);
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }

  init() {
    this.initCamera();
    this.initLights();
    this.cubes.create();

    this.raf = window.requestAnimationFrame(this.tick);
  }
}
