import * as THREE from 'three';

import vertexShader from '../shaders/vertex.glsl';
import fragmentShader from '../shaders/fragment.glsl';

class Cube {
  constructor(scene) {
    this.scene = scene;

    this.cubes = [];
    this.geometry;
    this.material;

    this.config = {
      cubeSize: 40,
      cubeCount: 1
    };

    this.uniforms = {
      u_time: { type: 'f', value: 0.2 },
      u_resolution: { type: 'v2', value: new THREE.Vector2() },
      u_amplitude: { type: 'f', value: 1.0 },
    };

    // this.attributes = {
    //   a_position: { type: 'f', value: 2.0 }
    // };
  }

  create() {
    // dispose previous cubes
    this.dispose();

    this.geometry = new THREE.BoxBufferGeometry(this.config.cubeSize, this.config.cubeSize, this.config.cubeSize);

    this.material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader
    });

    // this.geometry.setAttribute('a_position', new THREE.BufferAttribute(new Float32Array(0), 1));

    this.cube = new THREE.Mesh(this.geometry, this.material);
    this.scene.add(this.cube);
  }

  update() {
    // // increment global time
    this.cube.material.uniforms.u_time.value += 1/60;
    this.cube.material.uniforms.u_amplitude.value += Math.sin(1 + this.cube.material.uniforms.u_time.value);
    // this.attributes.a_position.needsUpdate = true;
  }

  init() {
    
  }

  dispose() {
    this.cube && this.scene.remove(this.cube);
    this.geometry && this.geometry.dispose();
    this.material && this.material.dispose();
  }
}

export default Cube;
