import * as THREE from 'three';

class Cube {
  constructor(scene) {
    this.scene = scene;

    this.points = null;
    this.geometry;
    this.material;

    this.config = {
      particles: 500000,
      pointSize: 2
    };

    this.positions = [];
    this.colors = [];
  }

  create() {
    // dispose previous cubes
    this.dispose();

    this.geometry = new THREE.BufferGeometry();
    this.color = new THREE.Color();

    const n = 500
    const n2 = n / 2; // particles spread in the cube

    let x;
    let y;
    let z;
    let vx;
    let vy;
    let vz;

    for (let i = 0; i < this.config.particles; i += 1) {
      // positions
      x = Math.random() * n - n2;
      y = Math.random() * n - n2;
      z = Math.random() * n - n2;

      this.positions.push(x, y, z);

      // colors
      vx = (x / n) + 0.5;
      vy = (y / n) + 0.5;
      vz = (z / n) + 0.5;

      this.color.setRGB(vx, vy, vz);
      
      this.colors.push(this.color.r, this.color.g, this.color.b);
    }

    this.geometry = new THREE.BufferGeometry();
    this.geometry.setAttribute('position', new THREE.Float32BufferAttribute(this.positions, 3));
    this.geometry.setAttribute('color', new THREE.Float32BufferAttribute(this.colors, 3));
    this.geometry.computeBoundingSphere();

    this.material = new THREE.PointsMaterial( { size: this.config.pointSize, vertexColors: THREE.VertexColors } );

    this.points = new THREE.Points(this.geometry, this.material);
    this.scene.add(this.points);
  }

  update() {
    const time = Date.now() * 0.001;

    this.points.rotation.x = time * 0.25;
    this.points.rotation.y = time * 0.5;
  }

  dispose() {
    this.points && this.scene.remove(this.points);
    this.geometry && this.geometry.dispose();
    this.material && this.material.dispose();
  }
}

export default Cube;
