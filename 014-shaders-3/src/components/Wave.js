import * as THREE from 'three';

class Cube {
  constructor(scene) {
    this.scene = scene;

    this.points = null;
    this.geometry;
    this.material;

    this.config = {
      amountX: 600,
      amountY: 1,
      amountZ: 300,
      delta: 10, // separation
      pointSize: 4,
      color: 0xff0000
    };

    this.positions = [];
    this.colors = [];

    this.time = 0;
  }

  create() {
    // dispose previous points
    this.dispose();

    let x;
    let y;
    let z;

    for (let ix = 0; ix < this.config.amountX; ix += 1) {
      for (let iy = 0; iy < this.config.amountY; iy += 1) {
        for (let iz = 0; iz < this.config.amountZ; iz += 1) {
          x = ix * this.config.delta - ((this.config.amountX * this.config.delta) / 2);
          y = iy * this.config.delta - ((this.config.amountY * this.config.delta) / 2);
          z = iz * this.config.delta - ((this.config.amountZ * this.config.delta) / 2);

          this.positions.push(x, y, z);
        }
      }
    }

    this.geometry = new THREE.BufferGeometry();
    this.geometry.setAttribute('position', new THREE.Float32BufferAttribute(this.positions, 3));
    // this.geometry.setAttribute('color', new THREE.Float32BufferAttribute(this.colors, 3));
    this.geometry.computeBoundingSphere();

    this.material = new THREE.PointsMaterial( { size: this.config.pointSize, vertexColors: THREE.VertexColors } );

    this.points = new THREE.Points(this.geometry, this.material);
    this.scene.add(this.points);
    this.points.rotation.y = 15;
    this.points.rotation.z = 3;
  }

  update() {
    this.time += 0.1;
    this.positions = [];

    let x;
    let y;
    let z;

    for (let ix = 0; ix < this.config.amountX; ix += 1) {
      for (let iy = 0; iy < this.config.amountY; iy += 1) {
        for (let iz = 0; iz < this.config.amountZ; iz += 1) {
          x = ix * this.config.delta - ((this.config.amountX * this.config.delta) / 2);
          y = (Math.sin((ix + this.time) * 0.2) * this.config.delta) + (Math.sin((iz + this.time) * 0.5) * 6);
          z = iz * this.config.delta - ((this.config.amountZ * this.config.delta) / 2);

          this.positions.push(x, y, z);
        }
      }
    }

    this.geometry.setAttribute('position', new THREE.Float32BufferAttribute(this.positions, 3));
    this.geometry.computeBoundingSphere();
    
    this.geometry.attributes.position.needsUpdate = true;
  }

  dispose() {
    this.points && this.scene.remove(this.points);
    this.geometry && this.geometry.dispose();
    this.material && this.material.dispose();
  }
}

export default Cube;
