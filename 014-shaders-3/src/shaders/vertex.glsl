uniform float u_time;
uniform float u_amplitude;

void main() {
  vec3 newPosition = position + u_amplitude;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(newPosition, 1.0);
}
