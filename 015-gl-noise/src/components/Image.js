import * as THREE from 'three'
import { TweenMax as TM } from 'gsap'

import vertexShader from '../shaders/vertex.glsl'
import fragmentShader from '../shaders/fragment.glsl'

class Image {
  constructor() {
    this.mouse = new THREE.Vector3();

    this.uniforms = {
      mouse: { type: 'f', value: new THREE.Vector3() },
      texture: { type: "t", value: THREE.ImageUtils.loadTexture("https://images.unsplash.com/photo-1478512611414-50d132a6dcd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80") }
    };
  }

  create() {
    const geometry = new THREE.PlaneGeometry(window.innerWidth, window.innerHeight, 100, 100);
    const material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      // fragmentShader
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.set(0, 0 , 0);
  }

  animate = () => {
    this.mesh.rotation.x += 0.01;
    this.mesh.rotation.y += 0.01;

    this.mesh.material.uniforms.mouse.value = this.mouse;
  }

  listeners() {
    window.addEventListener('mousemove', (event) => {
      this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    }, false)
  }

  init() {
    this.listeners();
    this.create();
  }
}

export default Image;
