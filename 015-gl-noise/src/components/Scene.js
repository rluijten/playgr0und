import * as THREE from 'three';
// import { TweenMax as TM } from 'gsap';

import vertexShader from '../shaders/vertex.glsl';
import fragmentShader from '../shaders/fragment.glsl';
import Snowflake from './SnowFlake';
import Image from './Image';

const perspective = 800;

export default class Scene {
  constructor() {
    this.DOM = {
      container: document.getElementById('stage')
    };

    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.DOM.container,
      alpha: true
    });

    this.uniforms = {
      u_time: { type: 'f', value: 0.2 },
      u_resolution: { type: 'v2', value: new THREE.Vector2() },
    };

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);
  }

  initCamera() {
    const fov =
      (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI;

    this.camera = new THREE.PerspectiveCamera(
      fov,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    this.camera.position.set(0, 0, perspective);
  }

  createSnowflake() {
    this.snowflake = new Snowflake({
      scene: this.scene
    });
    this.snowflake.init();

    this.scene.add(this.snowflake.mesh);
  }

  createImage() {
    this.image = new Image({
      scene: this.scene
    });
    this.image.init();

    this.scene.add(this.image.mesh);
  }

  update = () => {
    if (
      this.renderer === undefined ||
      this.scene === undefined ||
      this.camera === undefined
    )
      return;

    this.snowflake && this.snowflake.animate();

    requestAnimationFrame(this.update);

    this.renderer.render(this.scene, this.camera);
  }

  init() {
    this.initCamera();
    // this.createSnowflake();
    this.createImage();
    this.update();
  }
}
