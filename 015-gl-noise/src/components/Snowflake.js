import * as THREE from 'three'
import { TweenMax as TM } from 'gsap'

import vertexShader from '../shaders/vertex.glsl'
import fragmentShader from '../shaders/fragment.glsl'

class Snowflake {
  constructor() {
    this.mouse = new THREE.Vector3();

    this.uniforms = {
      mouse: { type: 'f', value: new THREE.Vector3() }
    };
  }

  create() {
    const geometry = new THREE.IcosahedronGeometry( 130, 5 );
    const material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader,
      fragmentShader
    });

    this.mesh = new THREE.Mesh(geometry, material);
    this.mesh.position.set(0, 0 , 0);
  }

  animate = () => {
    this.mesh.rotation.x += 0.01;
    this.mesh.rotation.y += 0.01;

    this.mesh.material.uniforms.mouse.value = this.mouse;
  }

  listeners() {
    window.addEventListener('mousemove', (event) => {
      this.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
      this.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    }, false)
  }

  init() {
    this.listeners();
    this.create();
  }
}

export default Snowflake;
