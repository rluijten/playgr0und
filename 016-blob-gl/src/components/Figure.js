import * as THREE from 'three';
import { TweenMax, Expo } from 'gsap';

import vertexShader from '../shaders/vertex.glsl';
import fragmentShader from '../shaders/fragment.glsl';

import image from '../assets/img/image.jpg';

export default class Figure {
  constructor(_options) {
    this.DOM = {
      image: _options.image
    }

    this.scene = _options.scene;

    this.DOM.image.style.opacity = 0;

    this.loader = new THREE.TextureLoader()
    // this.image = this.loader.load(this.DOM.image.dataset.image);
    this.image = this.loader.load(image);
    this.sizes = new THREE.Vector2(0, 0);
    this.mouse = _options.mouse;

    this.start();
    
    window.addEventListener('mousemove', this.onMouseMove)
    window.addEventListener('click', this.onClick);
  }

  start = () => {
    this.getSizes();
    this.createMesh();
    this.update();

    setTimeout(() => {
      TweenMax.to(this.mesh.material.uniforms.u_size, 2.5, {
        value: 35,
        ease: Expo.easeInOut
      });
      // TweenMax.to(this.mesh.material.uniforms.u_size, 2.5, {
      //   value: 110,
      //   ease: Expo.easeInOut,
      //   onComplete: () => {
      //     TweenMax.to(this.mesh.material.uniforms.u_size, 2.5, {
      //       value: 1,
      //       ease: Expo.easeInOut
      //     });
      //   }
      // });
    }, 1000);
  }

  getSizes = () => {
    const { width, height } = this.DOM.image.getBoundingClientRect();

    this.sizes.set(width, height);
  }

  createMesh() {
    this.uniforms = {
      u_image: { type: 't', value: this.image },
      u_mouse: { value: this.mouse },
      u_time: { value: 0 },
      u_size: { value: 0 },
      u_res: {
        value: new THREE.Vector2(window.innerWidth, window.innerHeight)
      }
    };

    this.geometry = new THREE.PlaneBufferGeometry(1, 1, 1, 1);
    this.material = new THREE.ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
      defines: {
        PR: window.devicePixelRatio.toFixed(1)
      }
    });

    this.mesh = new THREE.Mesh(this.geometry, this.material);
    this.mesh.scale.set(this.sizes.x, this.sizes.y, 1);

    this.scene.add(this.mesh);
  }

  // onClick = () => {

  // }

  update() {
    this.mesh.material.uniforms.u_time.value += 0.01;

    // TweenMax.to(12, this.mesh.material.uniforms.u_size, {
    //   value: 0,
    //   ease: Expo.easeInOut
    // });
    // this.mesh.material.uniforms.u_size.value += 0.02;
  }
}
