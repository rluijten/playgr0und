import * as THREE from 'three';
import { TweenMax as TM } from 'gsap'

import Figure from "./Figure";

const perspective = 800;

export default class Scene {
  constructor() {
    this.DOM = {
      container: document.getElementById('stage'),
      image: document.querySelector('.tile')
    }

    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.DOM.container,
      alpha: true
    });

    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.setLights();
    this.setCamera();

    this.intersectObject = null;
    this.mouse = new THREE.Vector2(0, 0);

    this.figure = new Figure({
      image: this.DOM.image,
      scene: this.scene,
      mouse: this.mouse
    });


    this.update();

    // window.addEventListener('mousemove', this.onMouseMove, false);
  }

  setLights() {
    const ambientlight = new THREE.AmbientLight(0xffffff, 2);
    this.scene.add(ambientlight);
  }

  setCamera() {
    const fov =
      (180 * (2 * Math.atan(window.innerHeight / 2 / perspective))) / Math.PI;

    this.camera = new THREE.PerspectiveCamera(
      fov,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    this.camera.position.set(0, 0, perspective);
  }

  update = () => {
    this.figure.update();

    requestAnimationFrame(this.update);

    this.renderer.render(this.scene, this.camera);
  }

  // onMouseMove = (e) => {
  //   TM.to(this.mouse, 0.5, {
  //     x: (e.clientX / window.innerWidth) * 2 - 1,
  //     y: -(e.clientY / window.innerHeight) * 2 + 1
  //   })
  // }
}
