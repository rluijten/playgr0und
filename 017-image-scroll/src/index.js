function easeOutQuad (t) {
  return t * (2 - t)
}

function lerp(a, b, n) {
  return (1 - n) * a + n * b;
}

class Scroll {
  constructor() {
    this.dom = {};
    this.dom.app = document.querySelector('.js-app');
    this.dom.el = document.querySelector('.js-scroll');
    this.dom.content = this.dom.el.querySelector('.js-content');

    this.state = {
      last: 0,
      ease: 0.1,
      current: 0,
    };
  }

  setHeight = () => {
    this.dom.content = this.dom.el.querySelector('.js-content');

    document.body.style.height = `${this.dom.content.offsetHeight}px`;
  }

  resize = () => {
    this.setHeight();
    this.scroll();
  }

  scroll = () => {
    this.state.current = window.scrollY;
  }

  render = () => {
    this.state.last = lerp(this.state.last, this.state.current, this.state.ease);

    if (this.state.last < 0.1) this.state.last = 0;

    this.dom.el.style.transform = `translate3d(0, -${this.state.last}px, 0)`;

    requestAnimationFrame(this.render);
  }

  addListeners() {
    window.addEventListener('scroll', this.scroll, { passive: true });
    window.addEventListener('resize', this.resize, { passive: true });
  }

  init() {
    this.setHeight();

    requestAnimationFrame(this.render);

    this.addListeners();
  }
}

class Image {
  constructor(el, index) {
    this.dom = {};
    this.dom.el = el;

    this.index = index;
    this.isEven = index % 2 === 0;
    this.isText = this.dom.el.classList.contains('text');
    this.bounds = el.getBoundingClientRect();
  }

  animate() {
    const input = scroll.state.last;
    const min = this.bounds.top - window.innerHeight;
    const max = this.bounds.top + this.bounds.height;
    const percentage = ((input - min) * 100) / (max - min);

    if (percentage >= 0 && percentage <= 100) {
      const inputLow = 0;
      const inputHigh = 100;
      const outputLowX = this.isEven ? -50 : 50;
      const outputHighX = this.isEven ? 50 : -50;
      const outputLowRotate = this.isText ? 30 : this.isEven ? 45 : -45;
      const outputHighRotate = this.isText ? -30 : this.isEven ? -45 : 45;
      let translateX = ((percentage - inputLow) / (inputHigh - inputLow)) * (outputHighX - outputLowX) + outputLowX;
      translateX = percentage >= 50 ? translateX * -1 : translateX;
      let rotate = ((percentage - inputLow) / (inputHigh - inputLow)) * (outputHighRotate - outputLowRotate) + outputLowRotate;
      rotate = percentage >= 50 ? rotate * -1 : rotate;

      this.dom.el.style.transform = `translateX(${translateX}px) rotateX(${rotate}deg) rotateY(${rotate}deg)`;
    }
  }

  render = () => {
    if (scroll.state.last + window.innerHeight >= this.bounds.top && scroll.state.last + window.innerHeight < this.bounds.top + this.bounds.height + window.innerHeight) {
      this.animate();
    }

    requestAnimationFrame(this.render);
  }

  init() {
    requestAnimationFrame(this.render);
  }
}

const scroll = new Scroll();
scroll.init();

const images = document.querySelectorAll('.js-img');
[...images].forEach((image, index) => {
  const img = new Image(image, index);
  img.init();
})