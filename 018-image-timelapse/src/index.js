function lerp(a, b, n) {
  return (1 - n) * a + n * b;
}

class Scroll {
  constructor() {
    this.dom = {};
    this.dom.app = document.querySelector('.js-app');
    this.dom.el = document.querySelector('.js-scroll');
    this.dom.content = this.dom.el.querySelector('.js-content');

    this.state = {
      last: 0,
      ease: 0.1,
      current: 0,
    };
  }

  setHeight = () => {
    this.dom.content = this.dom.el.querySelector('.js-content');

    document.body.style.height = `${this.dom.content.offsetHeight}px`;
  }

  resize = () => {
    this.setHeight();
    this.scroll();
  }

  scroll = () => {
    this.state.current = window.scrollY;
  }

  render = () => {
    this.state.last = lerp(this.state.last, this.state.current, this.state.ease);

    if (this.state.last < 0.1) this.state.last = 0;

    this.dom.el.style.transform = `translate3d(0, -${this.state.last}px, 0)`;

    requestAnimationFrame(this.render);
  }

  addListeners() {
    window.addEventListener('scroll', this.scroll, { passive: true });
    window.addEventListener('resize', this.resize, { passive: true });
  }

  init() {
    this.setHeight();

    requestAnimationFrame(this.render);

    this.addListeners();
  }
}

class Timelapse {
  constructor() {
    this.dom = {};
    this.dom.el = document.querySelector('.js-timelapse');
    this.dom.image = this.dom.el.querySelector('.js-image');
    this.dom.canvas = this.dom.el.querySelector('.js-canvas');

    this.state = {
      basePath: this.dom.el.dataset.image,
      bounds: this.dom.image.getBoundingClientRect(),
      total: 38,
      current: 0,
      last: 0
    };
  }

  setCanvas() {
    this.ctx = this.dom.canvas.getContext('2d');
    this.ctx.canvas.width  = this.state.bounds.width;
    this.ctx.canvas.height = this.state.bounds.height;
    this.ctx.fillRect(0, 0, this.dom.canvas.width, this.dom.canvas.height);
    this.ctx.drawImage(this.images[this.state.current], 0, 0, this.dom.canvas.width, this.dom.canvas.height);
  }

  setImages() {
    this.images = [];

    for (let i = 1; i < this.state.total; i += 1) {
      const img = new Image;
      const slug = `000${i}`;
      img.src = `${this.state.basePath}${slug.slice(-4)}.jpg`;
      this.images.push(img);
    }
  }

  render = () => {
    const isVisible = scroll.state.last + window.innerHeight >= this.state.bounds.top && scroll.state.last + window.innerHeight < this.state.bounds.top + this.state.bounds.height + window.innerHeight;

    if (isVisible) {
      const input = scroll.state.last;
      const min = this.state.bounds.top;
      const max = this.state.bounds.top + this.state.bounds.height;
      const percentage = ((input - min) * 100) / (max - min);

      if (percentage >= 0 && percentage <= 100) {
        const inputLow = 0;
        const inputHigh = 100;
        const outputLow = 1;
        const outputHigh = this.state.total;

        // calculate value between 1 and total of images
        this.state.last = Math.round(((percentage - inputLow) / (inputHigh - inputLow)) * (outputHigh - outputLow) + outputLow);

        // draw new image
        if (this.images[this.state.last] && this.state.current !== this.state.last) {
          this.ctx.fillRect(0, 0, this.dom.canvas.width, this.dom.canvas.height);
          this.ctx.drawImage(this.images[this.state.last], 0, 0, this.dom.canvas.width, this.dom.canvas.height);

          this.state.current = this.state.last;
        }
      }
    }

    requestAnimationFrame(this.render);
  }

  addListeners() {
    window.addEventListener('scroll', this.handleScroll, { passive: false });
  }

  init() {
    this.setImages();
    this.setCanvas();
    this.addListeners();

    requestAnimationFrame(this.render);

    // draw first image on load
    this.images[0].addEventListener('load', () => {
      this.ctx.fillRect(0, 0, this.dom.canvas.width, this.dom.canvas.height);
      this.ctx.drawImage(this.images[this.state.current], 0, 0, this.dom.canvas.width, this.dom.canvas.height);
    })
  }
}

const scroll = new Scroll();
scroll.init();

const timelapse = new Timelapse();
timelapse.init();