import { gsap } from 'gsap';

class Transition {
  constructor() {
    this.dom = {};
    this.dom.contents = document.querySelectorAll('.js-content');
  }

  animate() {
    this.tl = gsap.timeline({ paused: true });

    this.tl
      .addLabel('start')

      .set(this.dom.contents[1], {
        x: window.innerWidth,
        y: -window.innerHeight * 0.5,
        rotationX: -90,
        rotationY: -90
      })

      .to(this.dom.contents[0], {
        duration: 1.5,
        ease: 'Power3.easeInOut',
        x: -window.innerWidth,
        y: window.innerHeight * 0.5,
        rotationX: -45,
        rotationY: -45
      }, 'start')

      .to(this.dom.contents[1], {
        duration: 1.5,
        ease: 'Power3.easeInOut',
        x: 0,
        y: 0,
        rotationX: 0,
        rotationY: 0,
        autoAlpha: 1,
      }, 'start');

    this.tl.play();
  }

  init() {
    setTimeout(() => this.animate(), 1000);
  }
}

const transition = new Transition();
transition.init();