import gsap from 'gsap';
import Sniffer from 'sniffer';
import imagesLoaded from 'imagesloaded';
import VirtualScroll from 'virtual-scroll';

const math = {
  lerp: (a, b, n) => (1 - n) * a + n * b,
};

class Scroll {
  constructor() {
    this.vs = new VirtualScroll();

    this.vs.options.touchMultiplier = 2.5;
    this.vs.options.mouseMultiplier = 0.45;
    this.vs.options.firefoxMultiplier = 90;

    this.vs.options.passive = true;
    this.vs.options.limitInertia = false;

    this.section = document.querySelector('section');

    this.data = {
      last: 0,
      ease: 0.1,
      current: 0,
    };
  }

  render = () => {
    this.data.last = math.lerp(this.data.last, this.data.current, this.data.ease);

    this.data.abs = Math.abs(this.data.last);

    if (this.data.abs < 0.1) {
      this.data.last = 0;
    }

    this.section.style.transform = `translate3d(0px, ${this.data.last}px, 0px)`;
  }

  calc = (e) => {
    this.data.current += e.deltaY;
    this.data.current = Math.max((this.bounding.height - window.innerHeight) * -1, this.data.current);
    this.data.current = Math.min(0, this.data.current);
  }

  size = () => {
    this.bounding = this.section.getBoundingClientRect();
  }

  resize = () => {
    this.bounding = this.section.getBoundingClientRect();
  }

  events = () => {
    window.addEventListener('resize', this.resize, { passive: true });
  }

  on = () => {
    this.vs.on(this.calc);
    gsap.ticker.add(this.render);
  }

  off = () => {
    this.vs.destroy();
    gsap.ticker.remove(this.render);
    window.removeEventListener('resize', this.resize, { passive: true });
  }

  enable = () => {
    this.vs.on(this.calc);
  }

  disable = () => {
    this.vs.off();
  }

  init() {
    if (Sniffer.isDevice) {
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
      return;
    }

    imagesLoaded(this.section, () => {
      this.events();
      this.size();
      this.on();
    });
  }
}

const scroll = new Scroll();
scroll.init();