import gsap from 'gsap';
import VirtualScroll from 'virtual-scroll';
import imagesLoaded from 'imagesloaded';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

const lerp = (a, b, n) => (1 - n) * a + n * b;

class Scroll {
  constructor() {
    this.dom = {
      main: document.querySelector('main'),
      sections: document.querySelectorAll('[data-scroll]'),
    };

    this.state = {
      last: 0,
      ease: 0.1,
      height: 0,
      current: 0,
    };
  }

  setVs() {
    this.vs = new VirtualScroll();
    this.vs.options.passive = true;
    this.vs.options.limitInertia = false;
    this.vs.options.touchMultiplier = 2.5;
    this.vs.options.mouseMultiplier = 0.45;
    this.vs.options.firefoxMultiplier = 90;
  }

  render = () => {
    this.state.last = lerp(this.state.last, this.state.current, this.state.ease);
    this.state.abs = Math.abs(this.state.last);

    if (this.state.abs < 0.1) this.state.last = 0;
    if (this.isDevice) {
      this.state.current = this.dom.main.getBoundingClientRect().top;
      this.state.last = this.dom.main.getBoundingClientRect().top;
    } else {
      [...this.sections].forEach((section, index) => this.inViewport(index));
    }

    ScrollTrigger.update();
  }

  calc = (e) => {
    this.state.current += e.deltaY;
    this.state.current = Math.max((this.state.height - window.innerHeight) * -1, this.state.current);
    this.state.current = Math.min(0, this.state.current);
  }

  setCache() {
    this.sections = [];
    this.state.height = this.dom.main.getBoundingClientRect().height;
    this.dom.sections.forEach((el) => {
      const bounds = el.getBoundingClientRect();

      this.sections.push({ el, bounds });
    });
  }

  inViewport(index) {
    if (!this.sections) return;

    const cache = this.sections[index];
    const { top, height } = cache.bounds;
    const transform = Math.abs(this.state.last);
    const inView = transform >= top - window.innerHeight && transform < top + height;

    if (inView) {
      cache.el.style.transform = `translate3d(0px, ${transform * -1}px, 0px)`;
    } else {
      const resetTop = -top - height;
      cache.el.style.transform = `translate3d(0px, ${resetTop}px, 0px)`;
    }
  }

  resize = () => {
    [...this.dom.sections].forEach((section) => section.style.transform = 'translate3d(0px, 0px, 0px)');

    this.setCache();
    this.disable();
    this.enable();
  }

  events() {
    window.addEventListener('resize', this.resize, { passive: true });
  }

  on() {
    this.vs.on(this.calc);
    gsap.ticker.add(this.render);
  }

  off() {
    gsap.ticker.remove(this.render);
    if (this.vs) { this.vs.destroy(); }
    window.removeEventListener('resize', this.resize, { passive: true });
  }

  enable() {
    this.vs.on(this.calc);
  }

  disable() {
    this.vs.off();
  }

  setScrollTrigger() {
    ScrollTrigger.scrollerProxy(this.dom.sections[0], {
      scrollTop(value) {
        return arguments.length ? scroll.state.current = -value : Math.abs(scroll.state.last);
      },
      getBoundingClientRect() {
        return { top: 0, left: 0, width: window.innerWidth, height: window.innerHeight };
      },
      pinType: 'transform'
    });

    ScrollTrigger.defaults({
      scroller: this.dom.sections[0],
      start: 'top bottom',
      end: 'bottom top',
    });

    gsap.from('.heading', {
      scrollTrigger: {
        trigger: '.heading',
        scrub: true,
      },
      ease: 'none',
      yPercent: 100,
    });

    gsap.from('.line', {
      scrollTrigger: {
        trigger: '.line',
        scrub: true,
      },
      ease: 'none',
      scaleX: 0,
      transformOrigin: 'left center',
    });

    gsap.from('.box', {
      scrollTrigger: {
        trigger: '.box',
        scrub: true,
      },
      ease: 'none',
      rotation: 360,
      x: window.innerWidth - 100,
      transformOrigin: 'center center',
    });

    gsap.from('.panel', {
      scrollTrigger: {
        trigger: '.panel',
      },
      duration: 2,
      ease: 'Power3.easeOut',
      y: 300,
    });

    gsap.from('.panel-inner', {
      scrollTrigger: {
        trigger: '.panel-inner',
        scrub: true
      },
      ease: 'none',
      scale: 0.2,
    });

    gsap.from('.img-1', {
      scrollTrigger: {
        trigger: '.img-1',
      },
      duration: 2,
      ease: 'Power3.easeInOut',
      x: 300,
    });

    gsap.from('.img-2', {
      scrollTrigger: {
        trigger: '.img-2',
      },
      duration: 2,
      ease: 'Power3.easeInOut',
      x: -300,
    });

    gsap.fromTo('.img-3', {
      xPercent: -100
    }, {
      scrollTrigger: {
        trigger: '.img-3',
        scrub: true
      },
      ease: 'none',
      xPercent: 100
    });

    gsap.fromTo('.img-4', {
      scale: 0.2
    }, {
      scrollTrigger: {
        trigger: '.img-4',
        scrub: true
      },
      ease: 'none',
      scale: 1
    });

    gsap.fromTo('.img-5', {
      scale: 1
    }, {
      scrollTrigger: {
        trigger: '.img-5',
        scrub: true
      },
      ease: 'none',
      scale: 0.2
    });
  }

  init() {
    imagesLoaded(this.dom.sections, () => {
      if (!this.isDevice) {
        this.setVs();
        this.events();
        this.setCache();
        this.on();
        this.setScrollTrigger();
      };

      gsap.ticker.add(this.render);
    });
  }
}

const scroll = new Scroll();
scroll.init();
