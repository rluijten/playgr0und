import './assets/img/Camera-Glitch01.png';
import './assets/img/Camera-Glitch02.png';
import './assets/img/Camera-Glitch03.png';
import './assets/img/Camera-Glitch04.png';
import './assets/img/Camera-Glitch05.png';
import './assets/img/Camera-Glitch06.png';
import './assets/img/Camera-Glitch07.png';
import './assets/img/Camera-Glitch08.png';
import './assets/img/Camera-Glitch09.png';
import './assets/img/Camera-Glitch010.png';
import './assets/img/Camera-Glitch011.png';
import './assets/img/Camera-Glitch012.png';
import './assets/img/Camera-Glitch013.png';
import './assets/img/Camera-Glitch014.png';
import './assets/img/Camera-Glitch015.png';
import './assets/img/Camera-Glitch016.png';
import './assets/img/Camera-Glitch017.png';
import './assets/img/Camera-Glitch018.png';
import './assets/img/Camera-Glitch019.png';
import './assets/img/Camera-Glitch020.png';
import './assets/img/Camera-Glitch021.png';
import './assets/img/Camera-Glitch022.png';
import './assets/img/Camera-Glitch023.png';
import './assets/img/Camera-Glitch024.png';
import './assets/img/Camera-Glitch025.png';
import './assets/img/Camera-Glitch026.png';
import './assets/img/Camera-Glitch027.png';
import './assets/img/Camera-Glitch028.png';
import './assets/img/Camera-Glitch029.png';
import './assets/img/Camera-Glitch030.png';
import './assets/img/Camera-Glitch031.png';
import './assets/img/Camera-Glitch032.png';
import './assets/img/Camera-Glitch033.png';
import './assets/img/Camera-Glitch034.png';
import './assets/img/Camera-Glitch035.png';
import './assets/img/Camera-Glitch036.png';
import './assets/img/Camera-Glitch037.png';
import './assets/img/Camera-Glitch038.png';
import './assets/img/Camera-Glitch039.png';
import './assets/img/Camera-Glitch040.png';
import './assets/img/Camera-Glitch041.png';
import './assets/img/Camera-Glitch042.png';
import './assets/img/Camera-Glitch043.png';
import './assets/img/Camera-Glitch044.png';
import './assets/img/Camera-Glitch045.png';
import './assets/img/Camera-Glitch046.png';
import './assets/img/Camera-Glitch047.png';
import './assets/img/Camera-Glitch048.png';
import './assets/img/Camera-Glitch049.png';
import './assets/img/Camera-Glitch050.png';
import './assets/img/Camera-Glitch051.png';
import './assets/img/Camera-Glitch052.png';
import './assets/img/Camera-Glitch053.png';
import './assets/img/Camera-Glitch054.png';
import './assets/img/Camera-Glitch055.png';
import './assets/img/Camera-Glitch056.png';
import './assets/img/Camera-Glitch057.png';

class Sequence {
  constructor() {
    this.dom = {};
    this.dom.canvas = document.querySelector('#canvas');
    this.dom.btnStart = document.querySelector('.js-start');

    this.state = {
      total: 57,
      current: 0,
      loaded: 0
    };
  }

  setCanvas() {
    this.ctx = this.dom.canvas.getContext('2d');
    this.ctx.canvas.width = 825;
    this.ctx.canvas.height = 150;
  }

  load() {
    this.images = [];

    for (let i = 0; i < this.state.total; i += 1) {
      const img = new Image();
      const id = `0${i + 1}`;

      img.src = require(`./assets/img/Camera-Glitch${id.slice(-4)}.png`);

      img.onload = () => {
        this.images.push(img);
        this.state.loaded += 1;

        if (this.state.loaded === this.state.total - 1) {
          this.state.loaded = true;
        }
      };
    }
  }

  animate = () => {
    if (this.images[this.state.current]) {
      this.ctx.clearRect(0, 0, this.dom.canvas.width, this.dom.canvas.height);
      this.ctx.drawImage(this.images[this.state.current], 0, 0, this.dom.canvas.width, this.dom.canvas.height);
    }

    setTimeout(this.animate, 10);

    this.state.current += 1;

    if (this.state.current === this.state.total - 1) this.state.current = 0;
  }

  addListeners() {
    this.dom.btnStart.addEventListener('click', this.animate);
  }

  init() {
    this.addListeners();
    this.load();
    this.setCanvas();
  }
}

const sequence = new Sequence();
sequence.init();