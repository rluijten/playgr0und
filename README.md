# Playgr0und

## Development

1. Install module dependencies
```sh
  cd /example && yarn
```

2. Start the dev server
```sh
  yarn dev
```
